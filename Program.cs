﻿do
{
    Console.WriteLine("Enter Password - ");
    string passWord = Console.ReadLine();

    bool response = Validatation.validatePassword(passWord);
    if (response == false)
    {
        Console.WriteLine("Please Try Again\nThe Entered Password isn't strong");
    }
    else
    {
        Console.WriteLine("The Given Password is Strong\n");
        break;
    }
} while (true);