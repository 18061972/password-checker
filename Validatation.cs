﻿using System.Text.RegularExpressions;

internal class Validatation
{
    internal static bool validatePassword(string? password)
    {
        string pattern = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
        Regex reg = new Regex(pattern);
        bool result = reg.IsMatch(password);
        return result;
    }                       
}